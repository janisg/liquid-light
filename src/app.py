from bottle import route, run, template, view, static_file, request
import serial


@route('/')
@view('index')
def index():
    return dict()

@route('/static/<filepath:path>',name='static')
def static(filepath):
    return static_file(filepath, root='static')

@route('/ajax', method='POST')
def ajax():
    print 'red:',request.forms.get('red'),'blue:',request.forms.get('blue'),'green:', request.forms.get('green')
    return "Sis tekst tiek sutits atpakal uz brauzeri"

run(host='localhost', port=8080, reloader=True, interval=0.5)
