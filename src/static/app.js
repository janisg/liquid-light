var boardSize = 8;

$(function() { //on load
  console.log("starting jQuery");
  var currentColor = '#31859b';
  printBoard(boardSize);

  $("table tr td.cell").click(function() {
    $(this).css('background', currentColor);
    console.log("entering cell");
  });
  $("table tr td.cell").mouseleave(function() {
    console.log("leaving cell");
  });

   $("#cell-color").colorpicker({color:'#31859b', defaultPalette:'web'});

  $("#cell-color").on("change.color", function(event, color){
    console.log("color selected");
    currentColor = color;
  });
});

function printBoard(i_BoardSize) {
    var maxRow = parseInt(i_BoardSize);
    var maxCol = parseInt(i_BoardSize);
    var num = 1;

    var myTable = $("<table oncontextmenu=\"return false\"></table>").appendTo("#board");
    for (var row = maxRow - 1; row >= 0; row--) {
        var myRow = $("<tr></tr>").appendTo(myTable);
        for (var col = 0; col < maxCol; col++) {
            myRow.append("<td class='cell'>" + num + "</td>");
            num++;
        }
    }
}
