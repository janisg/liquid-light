<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Krāsas izvēle</title>
<link rel="shortcut icon" href="static/favicon.ico" />

<link rel="stylesheet" href="static/jquery-ui-1.11.2/jquery-ui.min.css" type="text/css">
<link rel="stylesheet" href="static/evoluteur-colorpicker/css/evol.colorpicker.min.css" type="text/css">
<link rel="stylesheet" href="static/liquid-light.css">

<script src="static/jquery-2.1.3.min.js" type="text/javascript"></script>
<script src="static/jquery-ui-1.11.2/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
<script src="static/evoluteur-colorpicker/js/evol.colorpicker.min.js" type="text/javascript" charset="utf-8"></script>
<script src="static/app.js" type="text/javascript" charset="utf-8"></script>

<meta name="viewport" content="width=device-width">
</head>

<body>
  <div id="container">
    <div id="left-panel">
      <div class="color-panel">
        Choose color:<br/><br/>
        <div id="cell-color"></div> 
      </div>
      <input class="submit-button" type="button" value="Start!"></input>
    </div>

    <div id="board"></div>
  </div>
</body>
</html>

